import http from '@chunpu/http'
//获取应用实例
const createRecycleContext = require("miniprogram-recycle-view");
const app = getApp();
/**
 * 注意：recycle-view 不是通过setData() 渲染的，
 * 需要独立使用createRecycleContext 方法创建的对象
 * 使用 append，splice，update，destory 进行列表数据的操作
 */
Page({
  name: 'recycleListPage', // 编辑某项后需要通过这个标记更新列表中对应的项
  data: {
    loadingText: '加载中',
  },
  variables: {
    pager: {
      page: 0,
      size: 10,
      hasMore: true // 是否已全部显示完
    }
  },
  onLoad: function () {
    wx.showModal({
      title: 'onLoad',
      content: 'onLoad called',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    let self = this;
    self.ctx = createRecycleContext({
      id: 'recycleId', // 对应 Html 中 list 组件的 id
      dataKey: 'recycleList', // wx:for 绑的数据
      page: this,
      itemSize: function (item, index) {
        // 这个 itemSize 只是给 JS 计算滚动用的，CSS 那要再写一次并且保持数值一致
        // JS 不会自动从 CSS 那读也不会给 HTML 设置这里的 width 和 height
        // 每个 item 的高度可以不一样
        let rs = {
          width: this.transformRpx(750),
          height: 150,
        };
        return rs;
      },
      useInPage: true,
      root: getCurrentPages()[getCurrentPages().length - 1],
    });
    self.refresh();
  },

  onPullDownRefresh: function () {
    let self = this;
    self.refresh();
  },
  onReachBottom: function () {
    let self = this;
    if (self.variables.pager.hasMore) {
      this.loadData();
    }
  },
  // 这个 scroll 空着就行 recycle-view 要求必须有
  onPageScroll(e) { },

  loadData: function (params = {}) {
    let self = this;
    self.variables.pager.page++;
    wx.showLoading();
    http.get('/api/more')
      .then(res => {
        if (!res.data.length) {
          self.variables.pager.hasMore = false;
        }
        this.ctx.append(res.data);
        wx.hideLoading();
      })
      .catch(error => {console.log(error)
        wx.hideLoading();
        wx.showModal({
          title: 'ERROR',
          content: error.errMsg,
          success(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      });
  },
  refresh: function () {
    let self = this;
    self.ctx.splice(0, self.ctx.comp.sizeArray.length);
    self.variables.pager.page = 0; // loadData 时会 ++
    self.variables.pager.hasMore = true;
    self.loadData();
  },
  // 更新列表中的某项
  update: function (data = {}, type = 'update') {
    let self = this;
    let idx = self.data.recycleList.findIndex(v => { // 待更新项在页面上 recycle-list 里的索引
      return data.id && v.id === data.id;
    });
    // 去除空值的属性
    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        const element = data[key];
        if (element == null) {
          delete data[key];
        }
      }
    }
    if (idx !== -1) {
      if ('update' == type) {
        self.ctx.update(idx, [{
          ...self.data.recycleList[idx],
          ...data
        }]);
      } else if ('delete' == type) {
        self.ctx.splice(idx, 1);
      }
    } else {
      console.error('列表里找不到待更新项：', idx, data, self.data.recycleList.map(v => v.id));
    }
  }
});