//app.js
import util from './utils/util'
import http from '@chunpu/http'
import Ready from 'min-ready'

const ready = Ready()

var JMessage = require('./libs/jmessage-wxapplet-sdk-1.4.3.min.js');
var jim = new JMessage({
  // debug : true
});

App({
  onLaunch: function () {
    util.promisify(wx.checkSession)().then(() => {
      console.log('session 生效')
      return this.getUserInfo()
    }).catch(err => {
      console.log(`自动登录失败, 重新登录`, err)
      return this.login()
    }).then(userInfo => {
      console.log('应用登录成功，开始登录极光...', userInfo)
      // 极光
      jim.init(userInfo.jgLogin).onSuccess(function(data) {
        console.log('INITSUCCEEDEDINITSUCCEEDEDINITSUCCEEDEDINITSUCCEEDEDINITSUCCEEDEDINITSUCCEEDED')
        jim.login({
          'username' : userInfo.user.username,
          'password' : userInfo.user.username
        }).onSuccess(function(data) {
          //data.code 返回码
          //data.message 描述
          //data.online_list[] 在线设备列表
          //data.online_list[].platform  Android,ios,pc,web
          //data.online_list[].mtime 最近一次登录时间
          //data.online_list[].isOnline 是否在线 true or false
          //data.online_list[].isLogin 是否登录 true or false
          //data.online_list[].flag 该设备是否被当前登录设备踢出 true or false
          console.log('LOGINSUCCEEDEDLOGINSUCCEEDEDLOGINSUCCEEDEDLOGINSUCCEEDEDLOGINSUCCEEDEDLOGINSUCCEEDEDLOGINSUCCEEDED')
          jim.onMsgReceive(function(data) {
            // data.messages[]
            // data.messages[].ctime_ms
            // data.messages[].msg_type 会话类型
            // data.messages[].msg_id
            // data.messages[].from_appey 单聊有效
            // data.messages[].from_username 单聊有效
            // data.messages[].from_gid 群聊有效
            // data.messages[].need_receipt
            // data.messages[].content
            // data.messages[].custom_notification.enabled
            // data.messages[].custom_notification.title
            // data.messages[].custom_notification.alert
            // data.messages[].custom_notification.at_prefix
            console.log('RECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVEDRECEIVED')
            console.log(data)
            wx.showModal({
              title: '提示',
              content: data.messages[0].content.msg_body.text,
              success (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
         });
        }).onFail(function(data){
          console.log('LOGINFAILEDLOGINFAILEDLOGINFAILEDLOGINFAILEDLOGINFAILEDLOGINFAILEDLOGINFAILEDLOGINFAILED')
        });
      }).onFail(function(data) {
        console.log('FAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILEDFAILED')
      });

    }).catch(err => {
      console.log(`应用服务 或 极光登录失败`, err)
    })
  },
  login () {
    console.log('登录')
    return util.promisify(wx.login)().then(({code}) => {
      console.log(`code: ${code}`)
      return http.get('/api/auth/thirdparty/weixin/wxapp?code='+code)
    }).then(() => {
      return this.getUserInfo()
    })
  },
  getUserInfo () {
    return http.get('/api/whoAmI').then(response => {
      let data = response.data
      if (data && typeof data === 'object') {
        this.globalData.userInfo = data
        ready.open()
        return data
      }
      return Promise.reject(response)
    })
  },
  ready (func) {
    ready.queue(func)
  },
  globalData: {
    userInfo: null
  }
})
